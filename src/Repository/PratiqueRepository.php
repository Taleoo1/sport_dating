<?php

namespace App\Repository;

use App\Entity\Pratique;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Pratique|null find($id, $lockMode = null, $lockVersion = null)
 * @method Pratique|null findOneBy(array $criteria, array $orderBy = null)
 * @method Pratique[]    findAll()
 * @method Pratique[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PratiqueRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Pratique::class);
    }

    // /**
    //  * @return Pratique[] Returns an array of Pratique objects
    //  */
    
    public function findDepSport(?string $value = null, ?string $design)
    {
      $entityManager = $this->getEntityManager();

      $searchDepSport = 'SELECT p, c, s FROM App\Entity\Pratique p INNER JOIN p.userid c INNER JOIN p.sportid s WHERE c.depart = :id AND s.design = :design';
      $searchDep = 'SELECT p, c, s FROM App\Entity\Pratique p INNER JOIN p.userid c INNER JOIN p.sportid s WHERE c.depart = :id';
      $searchSport = 'SELECT p, c, s FROM App\Entity\Pratique p INNER JOIN p.userid c INNER JOIN p.sportid s WHERE s.design = :design';
      if ($value !== null && $design !== null){
        $query = $entityManager->createQuery($searchDepSport)->setParameter('id', $value)->setParameter('design', $design);
      } else if ($value !== null) {
        $query = $entityManager->createQuery($searchDep)->setParameter('id', $value);
      } else if ($design !== null){
        $query = $entityManager->createQuery($searchSport)->setParameter('design', $design);
      } else {
        $query = $entityManager->createQuery("SELECT p, c, s FROM App\Entity\Pratique p INNER JOIN p.userid c INNER JOIN p.sportid s");
      }
      return $query->getResult();
    }
    

    /*
    public function findOneBySomeField($value): ?Pratique
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
