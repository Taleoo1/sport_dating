<?php

namespace App\Form;

use App\Entity\Sport;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

class SportFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
          ->add('design', EntityType::class, [
            'class' => Sport::class,
            'choice_label' => function(Sport $sport) {
              return sprintf('%s', $sport->__toString());
            },
            'label' => 'Sport pratiqué',
            'empty_data' => null,
            'required' => false
          ])
          ->add('newdesign', TextType::class, [
            "mapped" => false,
            "required" => false,
            'label' => "Autre Sport",
            'attr' => [
              'placeholder' => "Veuillez renseigner un sport que s'il n'est pas dans la liste"
            ]
          ]);
          
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Sport::class
        ]);
    }
}