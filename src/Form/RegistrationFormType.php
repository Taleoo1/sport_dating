<?php

namespace App\Form;

use App\Entity\User;
use App\Entity\Sport;
use App\Entity\Pratique;
use App\Form\SportFormType;
use App\Form\PratiqueFormType;
use Doctrine\ORM\Mapping\Entity;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\HttpClient\HttpClient;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Validator\Constraints\IsTrue;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;

class RegistrationFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
      
        $httpClient = HttpClient::create();
        $response = $httpClient->request('GET', 'http://geo.api.gouv.fr/departements')->toArray();
        $tempDepart = [];
        foreach($response as $data){
          array_push($tempDepart, $data["nom"] );
        }
        $depart = array_flip($tempDepart);
        foreach($tempDepart as $val) { 
          $depart[$val] = $val;
        };
        
        $builder
            ->add('email')
            ->add('nom')
            ->add('prenom')
            ->add('depart', ChoiceType::class, [
              'choices' => $depart,
              'empty_data' => 'rien',
              'required' => false
            ])
            ->add('plainPassword', PasswordType::class, [
                // instead of being set onto the object directly,
                // this is read and encoded in the controller
                'mapped' => false,
                'constraints' => [
                    new NotBlank([
                        'message' => 'Please enter a password',
                    ]),
                    new Length([
                        'min' => 6,
                        'minMessage' => 'Your password should be at least {{ limit }} characters',
                        // max length allowed by Symfony for security reasons
                        'max' => 4096,
                    ]),
                ],
            ])
            ->add('pratiques', PratiqueFormType::class, [
              'mapped' => false,
              'label' => false,
          ])
            ->add('sports', SportFormType::class,[
              'mapped' => false,
              'label' => false
            ]);

    }

//     public function configureOptions(OptionsResolver $resolver)
//     {
//         $resolver->setDefaults([
//             'data_class' => User::class,
//         ]);
//     }
}
