<?php

namespace App\Entity;

use App\Repository\PratiqueRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;


/**
 * @ORM\Entity(repositoryClass=PratiqueRepository::class)
 */
class Pratique
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=20)
     */
    private $niveau;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="pratiques")
     * @ORM\JoinColumn(nullable=false)
     */
    private $userid;

    /**
     * @ORM\ManyToOne(targetEntity=Sport::class, inversedBy="pratiques")
     * @ORM\JoinColumn(nullable=false)
     */
    private $sportid;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNiveau(): ?string
    {
        return $this->niveau;
    }

    public function setNiveau(string $niveau): self
    {
        $this->niveau = $niveau;

        return $this;
    }

    public function __toString(): string
    {
        return $this->getNiveau();
    }

    public function getUserid(): ?User
    {
        return $this->userid;
    }

    public function setUserid(?User $userid): self
    {
        $this->userid = $userid;

        return $this;
    }

    public function getSportid(): ?Sport
    {
        return $this->sportid;
    }

    public function setSportid(?Sport $sportid): self
    {
        $this->sportid = $sportid;

        return $this;
    }
}
