<?php

namespace App\Controller;

use App\Entity\Sport;
use App\Entity\Pratique;
use App\Form\PratiqueFormType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

class SecurityController extends AbstractController
{
    /**
     * @Route("/login", name="app_login")
     */
    public function login(AuthenticationUtils $authenticationUtils): Response
    {
        // if ($this->getUser()) {
        //     return $this->redirectToRoute('/test');
        // }

        // get the login error if there is one
        $error = $authenticationUtils->getLastAuthenticationError();
        // last username entered by the user
        $lastUsername = $authenticationUtils->getLastUsername();

        return $this->render('security/login.html.twig', ['last_username' => $lastUsername, 'error' => $error]);
    }

    /**
     * @Route("/logout", name="app_logout")
     */
    public function logout()
    {
        throw new \LogicException('This method can be blank - it will be intercepted by the logout key on your firewall.');
    }
    /**
     * @Route("/profile", name="app_profile")
     */
    public function editProfile(Request $request)
    {
      $user = $this->getUser();
      $form = $this->createFormBuilder()
            ->add('design', EntityType::class, [
                'class' => Sport::class,
                'choice_label' => function(Sport $sport) {
                  return sprintf('%s', $sport->__toString());
                },
                'label' => 'Sport pratiqué'])
            ->add('newdesign', TextType::class, [
              "mapped" => false,
              "required" => false,
              'label' => "Autre Sport",
              'attr' => [
                'placeholder' => "Veuillez renseigner un sport que s'il n'est pas dans la liste"
              ]
            ])
            ->add('pratiques', PratiqueFormType::class, [
              'mapped' => false,
              'label' => false
            ])
            ->add('save', SubmitType::class, ['label' => 'Rajouter ce sport à mon profil'])
            ->getForm();
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
          $entityManager = $this->getDoctrine()->getManager();
          $sport = new Sport();
          $pratique = new Pratique();

          
          if ($form->get('newdesign')->getData()){
            $sportExist = $this->getDoctrine()->getRepository(Sport::class)->findOneBy(['design' => ucfirst($form->get('newdesign')->getData())]);
            if (!$sportExist){
            $sport->setDesign(trim(ucfirst($form->get('newdesign')->getData())));
            $entityManager->persist($sport);
            $entityManager->flush();
            } else {
              $sport->setDesign($form->get('design')->getData()->getDesign());
            }
          } else {
            $sport->setDesign($form->get('design')->getData()->getDesign());
          }
          $pratique->setUserid($user);
          $pratique->setNiveau($form->get('pratiques')->getData());
          $pratique->setSportid($this->getDoctrine()->getRepository(Sport::class)->findOneBy(['design' => $sport->getDesign()]));
          $entityManager->persist($pratique);
          $entityManager->flush();
        }

      return $this->render('registration/profile.html.twig', ['form' => $form->createView(), 'user' => $user]);    
    }
}
