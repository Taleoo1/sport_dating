<?php

namespace App\Controller;

use App\Entity\User;
use App\Entity\Sport;
use App\Entity\Pratique;
use App\Form\RegistrationFormType;
use App\Security\AppAuthenticator;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Guard\GuardAuthenticatorHandler;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class RegistrationController extends AbstractController
{

    /**
     * @Route("/register", name="app_register")
     */
    public function register(Request $request, UserPasswordEncoderInterface $passwordEncoder, GuardAuthenticatorHandler $guardHandler, AppAuthenticator $authenticator): Response
    {
        $user = new User();
        $pratique = new Pratique();
        $sport = new Sport();
        $form = $this->createForm(RegistrationFormType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            // encode the plain password
            $user->setPassword(
                $passwordEncoder->encodePassword(
                    $user,
                    $form->get('plainPassword')->getData()
                )
            );

            //Save user
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($user);
            //Check if sport is already in DB
            if ($form->get('sports')->get('newdesign')->getData()){
              $sportExist = $this->getDoctrine()->getRepository(Sport::class)->findOneBy(['design' => ucfirst($form->get('sports')->get('newdesign')->getData())]);
              if (!$sportExist){
              $sport->setDesign(trim(ucfirst($form->get('sports')->get('newdesign')->getData())));
              $entityManager->persist($sport);
              } else {
                $sport->setDesign($form->get('sports')->getData()->getDesign());
              }
            } else {
              $sport->setDesign($form->get('sports')->getData()->getDesign());
            }
            $entityManager->flush();
            //Insert data in Pratiques table
            $pratique->setUserid($this->getDoctrine()->getRepository(User::class)->findOneBy(['email' => $form->get('email')->getData()]));
            $pratique->setNiveau($form->get('pratiques')->getData());
            $pratique->setSportid($this->getDoctrine()->getRepository(Sport::class)->findOneBy(['design' => $sport->getDesign()]));
            $entityManager->persist($pratique);
            $entityManager->flush();


            // do anything else you need here, like send an email

            return $guardHandler->authenticateUserAndHandleSuccess(
                $user,
                $request,
                $authenticator,
                'main' // firewall name in security.yaml
            );
        }

        return $this->render('registration/register.html.twig', [
            'registrationForm' => $form->createView(),
        ]);
    }
}
