<?php

namespace App\Controller;

use App\Entity\Sport;
use Symfony\Component\Asset\UrlPackage;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Asset\VersionStrategy\EmptyVersionStrategy;

class HomeController extends AbstractController
{
    /**
     * @Route("/", name="home")
     */
    public function index(): Response
    {
      $sports = $this->getDoctrine()
      ->getRepository(Sport::class)
      ->findAll();
      array_splice($sports, 8);



        return $this->render('home/index.html.twig', [
            'sports' => $sports,
        ]);
    }
}
