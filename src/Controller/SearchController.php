<?php

namespace App\Controller;

use App\Entity\User;
use App\Entity\Sport;
use App\Entity\Pratique;
use Symfony\Component\HttpClient\HttpClient;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SearchType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class SearchController extends AbstractController
{
    /** 
     * @Route("/search", name="search")
      */
    public function index(Request $request): Response
    {
      $this->denyAccessUnlessGranted('ROLE_USER');
      $userlist = $this->getDoctrine()->getRepository(Pratique::class)->findAll();


      $httpClient = HttpClient::create();
        $response = $httpClient->request('GET', 'http://geo.api.gouv.fr/departements')->toArray();
        $tempDepart = [];
        foreach($response as $data){
          array_push($tempDepart, $data["nom"] );
        }
        $depart = array_flip($tempDepart);
        foreach($tempDepart as $val) { 
          $depart[$val] = $val;
        };

      $form = $this->createFormBuilder()
            ->add('search', ChoiceType::class, [
              'choices' => $depart,
              'empty_data' => null,
              'required' => false
            ])
            ->add('design', EntityType::class, [
                'class' => Sport::class,
                'choice_label' => function(Sport $sport) {
                  return sprintf('%s', $sport->__toString());
                },
                'label' => 'Sport pratiqué',
                'empty_data' => null,
                'required' => false
                ])
            ->add('save', SubmitType::class, ['label' => 'Cherchez des gens !'])
            ->getForm();
        $form->handleRequest($request);
        if ($form->getData()['design']){
          $design = $form->getData()['design']->getDesign();
        } else {
          $design = null;
        }
        if ($form->isSubmitted() && $form->isValid()) {
          $userlist = $this->getDoctrine()->getRepository(Pratique::class)->FindDepSport($form->getData()['search'], $design);
        }

      
        return $this->render('search/index.html.twig', [
          'userlist' =>  $userlist,
          'form' => $form->createView()
        ]);
    }
}
